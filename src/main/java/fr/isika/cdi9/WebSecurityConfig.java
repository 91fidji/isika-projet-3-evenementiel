package fr.isika.cdi9;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import fr.isika.cdi9.services.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource datasource;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPwdEncoder;
	
	@Value("${spring.queries.users-query}")
	private String usersQuery;
	
	@Value("${spring.queries.roles-query}")
	private String rolesQuery;
		
	@Bean
	public UserDetailsService userDetailsService() {
		return new CustomUserDetailsService();
	}
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());		
		return authProvider;	
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery).dataSource(datasource).passwordEncoder(bCryptPwdEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/front/**").permitAll()
		.antMatchers("/admin/**").hasAuthority("Admin")
		.antMatchers("/admin/list").permitAll()
		.antMatchers("/organisateur/**").hasAuthority("Organisateur")
		.antMatchers("/prestataire/**").hasAuthority("Prestataire")
		.antMatchers("/participant/**").hasAuthority("Participant")		
		.antMatchers("/").permitAll()
		.anyRequest().permitAll()
		.and()
		.formLogin()
		//.loginPage("/login")
			.usernameParameter("email")
			.defaultSuccessUrl("/home")
			.permitAll()
		.and().logout().logoutSuccessUrl("/").permitAll();

	}

}
//.loginPage("/login").failureUrl("/login?error=true")
//.antMatchers("/").permitAll()
//.anyRequest().permitAll()