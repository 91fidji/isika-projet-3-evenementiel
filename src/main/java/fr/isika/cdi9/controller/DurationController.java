package fr.isika.cdi9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isika.cdi9.model.Duration;
import fr.isika.cdi9.repository.IDurationRepository;

@Controller
public class DurationController {
	
	@Autowired
	private IDurationRepository repo;
	
	@RequestMapping("/duration")
	public List<Duration> listDuration(){
		
		System.out.println((List<Duration>) repo.findAll() + " \n ");
		
		return (List<Duration>) repo.findAll();
	}
 
}
