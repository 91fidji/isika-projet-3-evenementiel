package fr.isika.cdi9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.repository.IPlaceRepository;

@Controller
public class PlaceController {
	
	@Autowired
	private IPlaceRepository repo;
	
	@RequestMapping("/place")
	public List<Place> listPlace() {
		System.out.println((List<Place>)repo.findAll() + " \n ");

		Place place = new Place();
		place.setName("test2");
		place.setCity("marseille");
		place.setStreetNbAndName("1");
		place.setZipCode("2");
		System.out.print("place"+place);
		Place temp = repo.saveAndFlush(place);
		System.out.print("temp"+ temp);
		
		
		return (List<Place>) repo.findAll();
	}
	
	
	
	

}
