package fr.isika.cdi9.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.isika.cdi9.model.Duration;
import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.EventType;
import fr.isika.cdi9.model.Picture;
import fr.isika.cdi9.model.Place;

import fr.isika.cdi9.model.PrestaType;
import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IDurationRepository;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IEventTypeRepository;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.repository.IPrestaTypeRepository;
import fr.isika.cdi9.repository.IPrestationRepository;
import fr.isika.cdi9.repository.IRoleRepository;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.services.UserService;

@Controller
public class LoginController {

	@Autowired
	private IEventTypeRepository eventTypeRepo;

	@Autowired
	private IPrestaTypeRepository prestaTypeRepo;

	@Autowired
	private IEventRepository eRepo;
	
	@Autowired
	private IPrestationRepository prestaRepo;

	@Autowired
	private IUserRepository userRepo;

	@Autowired
	private UserService userService;

	@Autowired
	private IPlaceRepository placeRepo;

	@Autowired
	private IRoleRepository roleRepo;
	
	@Autowired
	private IDurationRepository durationRepo;

	@GetMapping("")
	public String viewHomePage() {
		this.createDefaultEntities();

		return "front/okheey";
	}
	
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	
	public String login(Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getName() != "anonymousUser") {
			User user = userService.findUserByEmail(auth.getName());
			System.out.println("user" + user.getRoles());
			model.addAttribute(user);
		}
		return "/front/login";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		Place place = new Place();
		modelAndView.addObject("user", user);
		modelAndView.addObject(place);
		modelAndView.setViewName("registration");
		return modelAndView;
	}

	@GetMapping("403")
	public String error403() {
		return "error/403";
	}


	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(User user, BindingResult bindingResult) {
		System.out.println("user" + user);
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());

		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user", "Un utilisateur avec cet email existe déjà");
		}

		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("/");
		} else {
			Place userPlace = new Place();
			userPlace.setName("main");
			System.out.println(userPlace);
			user.setPlace(placeRepo.save(userPlace));
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "Le compte a bien été créé");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("front/okheey");
		}
		return modelAndView;
	}

	@GetMapping("/home")
	public String index() {
		String dashboard = "none";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userRepo.findByEmail(auth.getName());
		System.out.println("role 1 " + user);

		Role r = user.getRoles().stream().findAny().get();
		System.out.println("role 2" + r);

		String role = r.getRole();
		switch (role) {
		case "Admin":
			dashboard = "admin/list";
			break;
		case "Participant":
			dashboard = "participant/list";
			break;
		case "Prestataire":
			dashboard = "prestataire/list";
			break;
		case "Organisateur":
			dashboard = "organisateur/dashboard";
			break;
		case "none":
			dashboard = "index";
			break;
		}
		System.out.println(dashboard);
		return "redirect:/" + dashboard;
	}

	void createDefaultEntities() {
		Role roleOrganisateur = new Role();
		Role roleAdmin = new Role();
		Role roleParticipant = new Role();
		Role rolePrestataire = new Role();

		if (roleRepo.findByRole("Admin") == null) {
			roleAdmin.setRole("Admin");
			roleRepo.save(roleAdmin);
		}

		if (roleRepo.findByRole("Organisateur") == null) {
			roleOrganisateur.setRole("Organisateur");
			roleRepo.save(roleOrganisateur);
		}

		if (roleRepo.findByRole("Participant") == null) {
			roleParticipant.setRole("Participant");
			roleRepo.save(roleParticipant);
		}
		if (roleRepo.findByRole("Prestataire") == null) {
			rolePrestataire.setRole("Prestataire");
			roleRepo.save(rolePrestataire);
		}

		if (userService.findUserByEmail("organisateur@okheey.fr") == null) {
			User organisateur = new User();
			organisateur.setActive(1);
			organisateur.setEmail("organisateur@okheey.fr");
			organisateur.setPassword("organisateur");
			organisateur.setLastname("orga");
			organisateur.setAccountType("Organisateur");

			organisateur.setFirstname("organisateur");
			organisateur.setPhone("0242101312");
			Place placeOrganisateur = new Place();
			placeOrganisateur.setCity("Vannes");
			placeOrganisateur.setName("Main");
			placeOrganisateur.setStreetNbAndName("13");
			placeOrganisateur.setZipCode("56");
			organisateur.setPlace(placeRepo.save(placeOrganisateur));
			userService.saveUser(organisateur);
		}

		if (userService.findUserByEmail("p.emeline@hotmail.fr") == null) {
			User emeline = new User();
			emeline.setActive(1);
			emeline.setEmail("p.emeline@hotmail.fr");
			emeline.setPassword("emeline");
			emeline.setAccountType("Organisateur");
			emeline.setLastname("peyrot");
			emeline.setFirstname("emeline");
			emeline.setPhone("0142121312");
			emeline.setAccountType("organisateur");
			Place placeEmeline = new Place();
			placeEmeline.setCity("Paris");
			placeEmeline.setName("Main");
			placeEmeline.setStreetNbAndName("12");
			placeEmeline.setZipCode("12");
			emeline.setPlace(placeRepo.save(placeEmeline));
			userService.saveUser(emeline);
		}
//		if (userService.findUserByEmail("michel@gmail.com") == null) {
//			User participant = new User();
//
//			participant.setActive(1);
//			participant.setEmail("michel@gmail.com");
//			participant.setPassword("participant");
//			participant.setAccountType("participant");
//			participant.setLastname("Galabru");
//			participant.setAccountType("participant");
//			participant.setFirstname("Michel");
//			participant.setPhone("0141111312");
//			Place placeParticipant = new Place();
//			placeParticipant.setCity("Paris");
//			placeParticipant.setName("Main");
//			placeParticipant.setStreetNbAndName("5 rue de l'Eglise");
//			placeParticipant.setZipCode("75004");
//			participant.setPlace(placeRepo.save(placeParticipant));
//			userService.saveUser(participant);
//
//			Duration duration = new Duration();
//			duration.setDuration(2f);
//			duration.setStartDate(new Date());
//			duration.setEndDate(new Date());
//			duration.setFormat("Jour(s)");
//
//			Event event1 = new Event();
//			event1.setDuration(durationRepo.save(duration));
//			event1.setName("TeamBuilding Isika");
//			event1.setAccessibility(true);
//			event1.setDescriptionEvent("TeamBuilding travailler en équipe");
//			event1.setEventType("TeamBuilding");
//			event1.setNbOfParticipant(150);
//			event1.setPrice(100d);
//			event1.setPlace(placeParticipant);
//			event1.setOrganiser(userRepo.findByEmail("organisateur@okheey.fr"));
//			// event1.getPicture().setPathToPic("../../uploads/insertpicdefault.png");
//
//			HashSet<User> users = new HashSet<User>();
//			users.add(participant);
//			event1.setOtherUsers(users);
//			eRepo.save(event1);
//
//		}

		if (userService.findUserByEmail("lafinebouche@traiteur.fr") == null) {
			User prestataire = new User();
			prestataire.setActive(1);
			prestataire.setEmail("lafinebouche@traiteur.fr");
			prestataire.setPassword("prestataire");
			prestataire.setAccountType("prestataire");
			prestataire.setLastname("Presta");
			prestataire.setFirstname("Olivier");
			prestataire.setAccountType("prestataire");
			prestataire.setPhone("0141111812");
			Place placePrestataire = new Place();
			placePrestataire.setCity("Paris");
			placePrestataire.setName("Main");
			placePrestataire.setStreetNbAndName("4 impasse Joly");
			placePrestataire.setZipCode("59");
			prestataire.setPlace(placeRepo.save(placePrestataire));
			Prestation prestationT = new Prestation();
			prestationT.setCapacity(200);
			prestationT.setCost(50d);
			prestationT.setDescription("Buffet pour 200 personnes avec petits fours et champagne.");
			prestationT.setName("Traiteur & Co.");
			prestationT.setPlace(placePrestataire);
			prestationT.setPrestaType(prestaTypeRepo.findByPrestaType("Traiteur"));
			userService.saveUser(prestataire);
		}

		// Prestatype
		List<PrestaType> prestaType = prestaTypeRepo.findAll();
		if (prestaType.isEmpty()) {
			PrestaType prestaTypeLocation = new PrestaType();
			prestaTypeLocation.setPrestaType("Location salle");
			PrestaType prestaTypeTraiteur = new PrestaType();
			prestaTypeTraiteur.setPrestaType("Traiteur");
			PrestaType prestaTypeHebergement = new PrestaType();
			prestaTypeHebergement.setPrestaType("Hébergement");
			PrestaType prestaTypeAnimation = new PrestaType();
			prestaTypeAnimation.setPrestaType("Animation");
			PrestaType prestaTypeAutre = new PrestaType();
			prestaTypeAutre.setPrestaType("Autre");

			prestaTypeRepo.save(prestaTypeLocation);
			prestaTypeRepo.save(prestaTypeAnimation);
			prestaTypeRepo.save(prestaTypeHebergement);
			prestaTypeRepo.save(prestaTypeAutre);
			prestaTypeRepo.save(prestaTypeTraiteur);

		}

		if (userService.findUserByEmail("admin@okheey.fr") == null) {
			User admin = new User();
			admin.setActive(1);
			admin.setEmail("admin@okheey.fr");
			admin.setPassword("admin");
			admin.setAccountType("Admin");
			admin.setAccountType("admin");
			admin.setLastname("ad");
			admin.setFirstname("Admin");
			admin.setPhone("014210232");
			Place placeAdmin = new Place();
			placeAdmin.setCity("Bordeaux");
			placeAdmin.setName("Main");
			placeAdmin.setStreetNbAndName("13");
			placeAdmin.setZipCode("33");
			admin.setPlace(placeRepo.save(placeAdmin));
			userService.saveUser(admin);
		}

		if (userService.findUserByEmail("com@inocean.fr") == null) {
			User organisateur = new User();
			organisateur.setActive(1);
			organisateur.setEmail("com@inocean.fr");
			organisateur.setPassword("organisateur");
			organisateur.setLastname("Orga");
			organisateur.setFirstname("Emeline");
			organisateur.setPhone("02030405");
			organisateur.setAccountType("organisateur");
			Place placeOrganisateur = new Place();
			placeOrganisateur.setCity("Paris");
			placeOrganisateur.setName("Main");
			placeOrganisateur.setStreetNbAndName("13 rue Crozatier");
			placeOrganisateur.setZipCode("75001");
			organisateur.setPlace(placeRepo.save(placeOrganisateur));
			userService.saveUser(organisateur);
			
			
			Duration duration = new Duration();
			duration.setDuration(2f);		
			duration.setStartDate(new Date());
			duration.setEndDate(new Date());
			duration.setFormat("Jour(s)");

			Event event1 = new Event();
			event1.setDuration(durationRepo.save(duration));
			event1.setName("Lancement de produit Inocean");
			event1.setAccessibility(true);
			event1.setDescriptionEvent("Révélation Kona");
			event1.setEventType("Autre");
			event1.setNbOfParticipant(420);
			event1.setPrice(100d);
			event1.setPlace(placeOrganisateur);
			event1.setOrganiser(userRepo.findByEmail("com@inocean.fr"));
			// event1.getPicture().setPathToPic("../../uploads/insertpicdefault.png");

			HashSet<User> users = new HashSet<User>();
//			users.add(participant);
			event1.setOtherUsers(users);
			eRepo.save(event1);
									
		}

		List<EventType> eventType = eventTypeRepo.findAll();
		if (eventType.isEmpty() || eventType.size() < 5) {
			if (!eventType.isEmpty()) {
				eventTypeRepo.deleteAll();
			}
			EventType eventTypeMariage = new EventType();
			eventTypeMariage.setEventType("Mariage");
			EventType eventTypeAnniversaire = new EventType();
			eventTypeAnniversaire.setEventType("Anniversaire");
			EventType eventTypeTeamBuilding = new EventType();
			eventTypeTeamBuilding.setEventType("TeamBuilding");
			EventType eventTypeSeminaire = new EventType();
			eventTypeSeminaire.setEventType("Seminaire");
			EventType eventTypeOthers = new EventType();
			eventTypeOthers.setEventType("Autre");
			eventTypeRepo.save(eventTypeMariage);
			eventTypeRepo.save(eventTypeAnniversaire);
			eventTypeRepo.save(eventTypeOthers);
			eventTypeRepo.save(eventTypeTeamBuilding);
			eventTypeRepo.save(eventTypeSeminaire);

		}
		Duration duration = new Duration();
		duration.setDuration(3F);
		duration.setStartDate(new Date());
		duration.setEndDate(new Date());
		duration.setFormat("Jour(s)");
		List<Duration> durations = new ArrayList<Duration>();
		durations.add(duration);
		
//		Prestation prestation1 = new Prestation(1L,"Traiteur &Co",durationRepo.saveAll(durations) , 120d, 200, 
//				"Buffet pour 200 personnes avec petits-fours et champagne", 
//				placeRepo.save(new Place(12l, "13 rue Magny", "75014", "Paris", "Main")), prestaTypeRepo.save(prestaTypeRepo.findByPrestaType("Traiteur")),
//				userService.findUserByEmail("prestataire@okheey.fr"));
//		prestaRepo.save(prestation1);
		
//		Prestation prestation2 = new Prestation(2L,"Hotel de la Marne",durationRepo.saveAll(durations) , 120d, 42, 
//				"Hotel de charme près de la butte Montmartre", 
//				placeRepo.save(new Place(13l, "3 impasse Montmartre", "75018", "Paris", "Main")), prestaTypeRepo.save(prestaTypeRepo.findByPrestaType("Hébergement")),
//				userService.findUserByEmail("prestataire@okheey.fr"));
//		prestaRepo.save(prestation2);
	}
}
