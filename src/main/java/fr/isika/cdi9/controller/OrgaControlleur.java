package fr.isika.cdi9.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.isika.cdi9.model.Duration;
import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.EventType;
import fr.isika.cdi9.model.Picture;
import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IDurationRepository;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.repository.IPrestationRepository;
import fr.isika.cdi9.repository.IRoleRepository;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.repository.IEventTypeRepository;
import fr.isika.cdi9.repository.IPictureRepository;
import fr.isika.cdi9.services.EventService;
import fr.isika.cdi9.services.PrestationService;
import fr.isika.cdi9.services.UserService;

@Controller
@RequestMapping("/organisateur/")
public class OrgaControlleur {

	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/resources/static/upload";

	@Autowired
	private EventService eventService;

	@Autowired
	private PrestationService prestaService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private IPrestationRepository prestaRepo;

	@Autowired
	private IEventTypeRepository eventTypeRepo;

	@Autowired
	private IPlaceRepository placeRepo;

	@Autowired
	private IEventRepository eRepo;

	@Autowired
	private IDurationRepository durationRepo;

	@Autowired
	private IPictureRepository picRepo;

	private IRoleRepository roleRepo;
	
	
	@Transactional
	@GetMapping("dashboard")
	public String displayOrgaDashHome(Model modele) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();

		}
		List<Event> eList = eRepo.findAll();

		for (Event e : eList) {
			e.setNbInscrit(e.getOtherUsers().size());
		}
		modele.addAttribute("role", role);
		

		System.out.println("role "+user.getFirstname());
		List<Event> listEvents = eventService.findAllCommingEvent(user);
		modele.addAttribute("eventList", listEvents);
		modele.addAttribute("user", user);
		return "organisateur/organisateur";	
	}

	@GetMapping("addEvent")
	public String displayCreationEvent(Model modele) {
		List<Prestation> listPrestations = prestaService.findAllCommingPrestation();

		List<EventType> eventType = eventTypeRepo.findAll();
		System.out.println("liste" + eventType.size());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();

		}
		modele.addAttribute("role", role);
		
		modele.addAttribute("event", new Event());
		modele.addAttribute("listEventType", eventType);
		modele.addAttribute("duration", new Duration());
		modele.addAttribute("place", new Place());
		modele.addAttribute("listPrestations", listPrestations);
		return "organisateur/eventCreation";
	}

	@PostMapping("saveEvent")
	public String saveEvent(Event event, String eventType, Picture picture, Prestation prestation,
			@RequestParam("files") MultipartFile[] files) {
		List<Prestation> prestations = event.getPrestation();
		System.out.println("pr"+prestation);
		if (prestations == null) {
			prestations = new ArrayList<Prestation>();
		} 
		System.out.println("evenement" + event.getEventType());
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(authentication.getName());
		event.setOrganiser(user);
		Place placeT = new Place();
		event.setPrestation(prestations);
		EventType eT = eventTypeRepo.findByEventType(event.eventType);
		System.out.println("eventType : " + eT);
		event.setEventType(eT.getEventType().toString());
		placeT.setCity(event.getPlace().getCity());
		event.setPlace(placeRepo.save(placeT));

		Duration durationT = new Duration();
		durationT.setStartDate(event.getDuration().getStartDate());
		durationT.setEndDate(event.getDuration().getEndDate());
		event.setDuration(durationRepo.save(durationT));
		System.out.println(event.getDuration().getStartDate());

		StringBuilder fileName = new StringBuilder();
		MultipartFile file = files[0];
		Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
		fileName.append(file.getOriginalFilename());
		try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		picture.setPathToPic(fileName.toString());
		
		event.setPicture(picRepo.save(picture));
		eRepo.save(event);

		return ("redirect:dashboard");
	}

	@GetMapping("show/{id}")
	public String showEventPicture(@PathVariable("id") long id, Model model) {
		Event event = eRepo.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid provider Id:" + id));

		model.addAttribute("event", event);

		return "event/showEventPicture";
	}
	

}
