package fr.isika.cdi9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.repository.IRoleRepository;

@Controller
@RequestMapping("/role/")
public class RoleController {

	
	private final IRoleRepository roleRepo;
	
	@Autowired
	public RoleController(IRoleRepository roleRepository) {
		this.roleRepo = roleRepository;
	}
	
	@GetMapping("list")
	public String listRole(Model model) {
		List<Role> eRole = roleRepo.findAll();
		Long nombreRoles = roleRepo.count();
		
		if(eRole.size()==0) {
			eRole=null;
		}
		
		model.addAttribute("roleList", eRole);
		model.addAttribute("nombreRoles", nombreRoles);
		
		return "role/listRoles";		
	}
	
	@GetMapping("add")	
	public String showAddRoleForm() {
		return "role/addRole";
	}	
	
	@PostMapping("add")
	public String addRole(@RequestParam("role") String role) {
		System.out.println("role : " + role);
		Role newRole = new Role(role);
		Role roleSaved = roleRepo.save(newRole);
		System.out.println("role sauvé : " + roleSaved);
		return "redirect:list";
	}
	
	
	
	
	
	
}
