package fr.isika.cdi9.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IPrestationRepository;
import fr.isika.cdi9.repository.IRoleRepository;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.services.UserService;

@Controller
@RequestMapping("/admin/")
public class AdminController {

	@Autowired
	private IEventRepository eventRepo;

	@Autowired
	private IPrestationRepository prestaRepo;

	@Autowired
	private IUserRepository userRepo;

	@Autowired
	private IRoleRepository roleRepo;

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	private UserService userService;

	@GetMapping("list")
	public String listEvent(Model model) {

		List<Event> eList = eventRepo.findAll();
		List<Prestation> prestaList = prestaRepo.findAll();
		List<User> userList = userRepo.findAll();
		List<Role> eRole = roleRepo.findAll();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();
		}
		
		for (Event e : eList) {
			e.setNbInscrit(e.getOtherUsers().size());
		}
		model.addAttribute("role", role);
		model.addAttribute("roleList", eRole);
		model.addAttribute("prestaList", prestaList);
		model.addAttribute("eventList", eList);
		model.addAttribute("userList", userList);
		
		System.out.println(userList);

		return "admin/admin";
	}

	@GetMapping("AjoutUtilisateur")
	public String viewAddUser() {
		return "admin/AjoutUtilisateur";
	}

	@GetMapping("enable/{id}/{email}")
	// @ResponseBody
	public String enableUserAcount(@PathVariable("id") Long id, @PathVariable("email") String email) {
		
		sendEmail(email, true);
		
		User user = userRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		user.setActive(1);
		userRepo.save(user);
		return "redirect:../../list";
	}

	@GetMapping("disable/{id}/{email}")
	// @ResponseBody
	public String disableUserAcount(@PathVariable("id") Long id, @PathVariable("email") String email) {
		
		sendEmail(email, false);

		User user = userRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		user.setActive(0);
		userRepo.save(user);
		return "redirect:../../list";
	}

	@PostMapping("updateRole")
	public String updateUserRole(@RequestParam("id") Long id, @RequestParam("newRole") String newRole) {

		User user = userRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id: " + id));
		Role userRole = roleRepo.findByRole(newRole);
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userRepo.save(user);

		return "redirect:list";

	}

	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	// @ResponseBody
	public String deleteUserAcount(@PathVariable("id") Long id) {
		User user = userRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		// user.setActive(1);
		userRepo.delete(user);
		return "redirect:../list";
	}
	
	void sendEmail(String email, boolean state) {
		SimpleMailMessage msg = new SimpleMailMessage();
		
		msg.setTo(email);
		
		if(state == true) {
			msg.setSubject("Votre compte a été activé");
			msg.setText("Bonjour, \n votre compte a bien été activé. " + "\n\nConnectez vous via cette adresse : http://localhost:8080/login" + "\n\nBien à vous");
		}else {
			msg.setSubject("Votre compte a été désactivé");
			msg.setText("Bonjour, \n votre compte a été désactivé. ");			
		}
		
		javaMailSender.send(msg);
	}

}
