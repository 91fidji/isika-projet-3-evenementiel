package fr.isika.cdi9.controller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.ModelAndView;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.Picture;
import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.model.PrestaType;
import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.repository.IPrestaTypeRepository;
import fr.isika.cdi9.repository.IPrestationRepository;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.services.UserService;

@Controller
@RequestMapping("/prestataire/")
public class PrestataireController {

	public static String uploadDirectory = System.getProperty("user.dir")+"/src/main/resources/static/uploads";

	@Autowired
	private UserService userService;

	@Autowired
	private IEventRepository eventRepo;

	@Autowired
	private IPrestationRepository prestaRepo;

	@Autowired
	private IPrestaTypeRepository prestaTypeRepo;
	
	@Autowired
	private IPlaceRepository placeRepo;
	
	@Autowired
	private IUserRepository userRepo;

	@Transactional
	@GetMapping("list")
	public String listEvent(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();
		}

		List<Event> eList = eventRepo.findAll();
		List<Prestation> prestaList = prestaRepo.findPrestaByServiceProviderById(user);
		for (Event e : eList) {
			e.setNbInscrit(e.getOtherUsers().size());
		}
		
		model.addAttribute("role", role);

		model.addAttribute("prestaList", prestaList);
		model.addAttribute("eventList", eList);

		return "prestataire/prestataire";
	}

	@GetMapping("add_prestation") 
	public String viewAddPrestation(Model model) {	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();

		}
		model.addAttribute("role", role);

		List<PrestaType> prestaTypes = prestaTypeRepo.findAll();
		System.out.println(prestaTypes);
		
		model.addAttribute("prestation", new Prestation());
		model.addAttribute("prestaTypeList", prestaTypes);
//		model.addAttribute("duration", new Duration());
		model.addAttribute("place", new Place());
//		
		System.out.println(model);


		return "prestataire/add_prestation";
	}
	
	
	@PostMapping("add_prestation")
	public String processPrestation(Prestation prestation, Place place, @RequestParam("files") MultipartFile[] files, @RequestParam("newType") String newType) {

		System.out.println(prestation);
//		prestation.setPlace(placeRepo.saveAndFlush(place));

		// créer l'image
		Picture p = new Picture();
		System.out.println(prestation.getPicture());
		////Début Upload

		StringBuilder fileName = new StringBuilder();
		MultipartFile file = files[0];
		Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());

		fileName.append(file.getOriginalFilename());
		try {
			Files.write(fileNameAndPath, file.getBytes()); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		p.setName("photo");
		p.setPathToPic(fileName.toString());
		prestation.setPicture(p);
		System.out.println(p.getPathToPic());

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.findUserByEmail(auth.getName());
		prestation.setServiceProvider(user);

		PrestaType prestaType = prestaTypeRepo.findByPrestaType(newType);
		prestation.setPrestaType(prestaType);
		placeRepo.save(prestation.getPlace());
		
		System.out.println("add : "+ prestation);
		
		prestaRepo.save(prestation);
		
		return "redirect:list";


		///Fin Upload

		
	}

	
	@GetMapping("show/{id}")
	public String showPrestationDetails(@PathVariable("id") long id, Model model) {
		Prestation prestation = prestaRepo.findById(id) .orElseThrow(()->new
				IllegalArgumentException("Invalid provider Id:" + id));
		model.addAttribute("prestation", prestation);
		return "prestataire/showPrestation";
	}

		
	@GetMapping("update_prestation/{id}")
	public String showPrestationFormToUpdate(@PathVariable("id") long id, Model model) {
		List<PrestaType> prestaTypes = prestaTypeRepo.findAll();
		model.addAttribute("prestaTypeList", prestaTypes);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();

		}
		model.addAttribute("role", role);
		
		Prestation prestation = prestaRepo.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid prestation Id:" + id));
		
		model.addAttribute("prestation", prestation);
		return "prestataire/update_prestation";
	}


	@PostMapping("update_prestation")
	public String updatePrestation(@Valid Prestation prestation, BindingResult result, Model model, @RequestParam("id") Long id, @RequestParam("newType") String newType) {
		System.out.println("model  post meth : " +model);
		System.out.println("prestation post: "+prestation);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		prestation.setServiceProvider(user);

		
		System.out.println(prestation.getPlace());
		System.out.println(prestation.getPrestaType());
		
		prestation.setPlace(user.getPlace());
//		placeRepo.save(user.getPlace());
//		placeRepo.save(prestation.getPlace());
		
		PrestaType prestaType =  prestaTypeRepo.findByPrestaType(newType);
		prestation.setPrestaType(prestaType);
		
		if (result.hasErrors()) {
			model.addAttribute("prestation", prestation);
			
			return "prestataire/update_prestation";
		}
		prestaRepo.save(prestation);
		return "redirect:list";
		}
	

	


	@PostMapping("delete") 
	public String deletePrestation(@RequestParam("id_presta") long idDeMaPresta) {

		System.out.println(idDeMaPresta);
		prestaRepo.removeById(idDeMaPresta);
		return "redirect:list";
	}






}
