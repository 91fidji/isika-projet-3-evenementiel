package fr.isika.cdi9.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.services.UserService;

@Controller
@RequestMapping("participant/")
public class ParticipantController {

	
	@Autowired
	private IEventRepository repoEvent;
	
	@Autowired
	private IUserRepository repoUser;
	
	@Autowired
	private UserService userService;
	

	@GetMapping("/list")
	public String listParticipant(Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		String role = "";
		
		if (user.getEmail() != "anonymousUser") {
			role = user.getRoles().stream().findFirst().get().getRole();

		}
		model.addAttribute("role", role);
		
		List<Event> listEvent = repoEvent.findAll();
		List<Event> listEventParticipant = repoEvent.findAllEventByParticipantId(user);
	
		
		System.out.println(listEventParticipant);
		
		model.addAttribute("eventList", listEvent);
		model.addAttribute("eventListParticipant", listEventParticipant);
		return "participant/participant";
	}
	
	@RequestMapping(value="register/{id}", method = RequestMethod.POST)
	public String registration(@PathVariable("id") Long id) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repoUser.findByEmail(auth.getName());		
		HashSet<User> users = new HashSet<User>();
		users.add(user);
		Event event = repoEvent.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid User Id: " + id));
		event.setOtherUsers(users);
		repoEvent.save(event);
		
	
		return "redirect:../list";

	}
	

}
