package fr.isika.cdi9.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.isika.cdi9.model.Duration;
import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IUserRepository;
import fr.isika.cdi9.services.UserService;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IEventTypeRepository;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.repository.IPrestaTypeRepository;
import fr.isika.cdi9.repository.IRoleRepository;



@Controller
public class AppController {
	@Autowired
	private IUserRepository repo;	

	@Autowired
	private IPlaceRepository placeRepo;


	@Autowired
	private IEventRepository eRepo;

	@Autowired
	private IUserRepository userRepo;


	@Autowired
	private IRoleRepository roleRepo;
	
	@Autowired
	private UserService userService;	

	

	
	@GetMapping("/inscription")
	public String showSignupForm(Model model) {
		model.addAttribute("place", new Place());
		model.addAttribute("user", new User());
		return "front/inscription";
	}
	


	@PostMapping("/process_register")
	public String processRegistration(User user, Place place) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String passwordEncoded = encoder.encode(user.getPassword());
		user.setPassword(passwordEncoded);
		place.setName("principale");
		Place temp = placeRepo.saveAndFlush(place);
		user.setPlace(temp);
		repo.save(user);
		return "admin";
	}

	@GetMapping("/list_users")
	public String viewUserList(Model model) {
		List<User> listUsers = repo.findAll();
		model.addAttribute("listUsers", listUsers);
		return "list_users";
	}	
	
	@GetMapping("/connexion")
	public String goToLoginPage(Model model) {
		model.addAttribute("user", new User());
		return "front/login";
	}

	@GetMapping("/mentionsLegales")
	public String goToMentionLegales(Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		
		if (auth.getName() != "anonymousUser") {
			User user = repo.findByEmail(auth.getName());
			String role = user.getRoles().stream().findFirst().get().getRole();
			model.addAttribute("role",role);
		}
		
		return "front/mentionsLegales";
	}

}
