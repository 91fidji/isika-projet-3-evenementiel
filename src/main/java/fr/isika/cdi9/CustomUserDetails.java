package fr.isika.cdi9;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IEventRepository;

public class CustomUserDetails implements UserDetails {
	
	private User user;
	
	@Autowired
	private IEventRepository repo;

	public CustomUserDetails(User user) {
		super();
		this.user = user;
		createEvent();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return user.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
	public String getFullName() {
		return user.getFirstname()+ " "+ user.getLastname(); 
	}
	
	public void createEvent() {
		
		
		
		Event event = new Event();
		
		event.setName("testName");
		event.setNbOfParticipant(50);
		event.setDescriptionEvent("testDescription");
		event.setPrice(500d);
		event.setAccessibility(true);
		
		repo.save(event);
		
	}

}
