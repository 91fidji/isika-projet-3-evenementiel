package fr.isika.cdi9.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="presta_types")
public class PrestaType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length= 100)
	private String prestaType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrestaType() {
		return prestaType;
	}

	public void setPrestaType(String prestaType) {
		this.prestaType = prestaType;
	}

	@Override
	public String toString() {
		return "PrestaType [id=" + id + ", prestaType=" + prestaType + "]";
	}

	
	
}
