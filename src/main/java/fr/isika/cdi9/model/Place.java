package fr.isika.cdi9.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="places")
public class Place {
	
	@Override
	public String toString() {
		return "Place [name=" + name + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=true, length = 250)
	private String streetNbAndName;
	
	@Column(nullable=true, length = 40)
	private String zipCode;
	
	@Column(nullable=true, length = 60)
	private String city;
	
	@Column(nullable=true, length = 250)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreetNbAndName() {
		return streetNbAndName;
	}

	public void setStreetNbAndName(String streetNbAndName) {
		this.streetNbAndName = streetNbAndName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Place() {
		
	}

	public Place(Long id, String streetNbAndName, String zipCode, String city, String name) {
		super();
		this.id = id;
		this.streetNbAndName = streetNbAndName;
		this.zipCode = zipCode;
		this.city = city;
		this.name = name;
	}
	
}
