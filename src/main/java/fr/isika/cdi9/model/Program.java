package fr.isika.cdi9.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="programs")
public class Program {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length= 500)
	private String description;
	
	@Column(nullable=true, length= 500)
	private String pathToPDFProgram;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPathToPDFProgram() {
		return pathToPDFProgram;
	}

	public void setPathToPDFProgram(String pathToPDFProgram) {
		this.pathToPDFProgram = pathToPDFProgram;
	}
	

}
