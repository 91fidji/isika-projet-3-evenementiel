package fr.isika.cdi9.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pictures")
public class Picture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = true, length = 50)
	private String name;
	
	@Column(nullable = true, length = 500)
	private String pathToPic;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = true)
	private Event eventCree;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPathToPic() {
		return pathToPic;
	}

	public void setPathToPic(String pathToPic) {
		this.pathToPic = pathToPic;
	}

	public Event getEventCree() {
		return eventCree;
	}

	public void setEventCree(Event eventCree) {
		this.eventCree = eventCree;
	}

	
	
	
	
}
