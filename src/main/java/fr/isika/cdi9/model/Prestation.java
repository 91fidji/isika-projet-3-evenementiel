package fr.isika.cdi9.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prestations")
public class Prestation {

	@Id // clef primaire
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, length = 50)
	private String name;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(nullable = true, name = "prestation_id")
	private List<Duration> disponibility;

	@Column(nullable = false)
	private Double cost;

	@Column(nullable = false)
	private Integer capacity;

	@Column(nullable = true, length = 500)
	private String description;

	@OneToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(nullable = true)
	private Place place;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(nullable = true)
	private PrestaType prestaType;

	@Override
	public String toString() {
		return "Prestation [id=" + id + ", name=" + name + ", capacity=" + capacity + "]";
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(nullable = true)
	private Picture picture;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = true)
	private User serviceProvider;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Duration> getDisponibility() {
		return disponibility;
	}

	public void setDisponibility(List<Duration> disponibility) {
		this.disponibility = disponibility;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public PrestaType getPrestaType() {
		return prestaType;
	}

	public void setPrestaType(PrestaType prestaType) {
		this.prestaType = prestaType;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public User getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(User serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Prestation() {
		
	}

	public Prestation(Long id, String name, List<Duration> disponibility, Double cost, Integer capacity,
			String description, Place place, PrestaType prestaType, User serviceProvider) {
		super();
		this.id = id;
		this.name = name;
		this.disponibility = disponibility;
		this.cost = cost;
		this.capacity = capacity;
		this.description = description;
		this.place = place;
		this.prestaType = prestaType;
		this.serviceProvider = serviceProvider;
	}
	
	
	

}
