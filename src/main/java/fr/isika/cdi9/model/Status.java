package fr.isika.cdi9.model;

public enum Status {
	
	FORTHCOMING{public String toString() {
		return "A venir";}}, 
	INPROGRESS{public String toString() {
		return "En cours";}}, 
	ENDED{public String toString() {
		return "Terminé";}}
}
