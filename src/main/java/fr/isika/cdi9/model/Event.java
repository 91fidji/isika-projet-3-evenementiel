package fr.isika.cdi9.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Table(name = "events")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = true, length = 50)
	private String name;

	@Column(nullable = true)
	private Integer nbOfParticipant;

	@Column(nullable = true, length = 500)
	private String descriptionEvent;

	@Column(nullable = true)
	private Double price;

//	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@Column(nullable = true)
	public String eventType;

	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = true, name="estatus_id")
	private Status status;

	@OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.ALL)
	@JoinColumn(nullable = true, name="eduration_id")
	private Duration duration;

	@Column(nullable = true)
	private Boolean accessibility;
	
	@OneToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(nullable=true, name="eprogram_id")
	private Program program;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(nullable=true, name="eplace_id")
	private Place place;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(nullable=true, name="event_id")
	private List<Task> tasks;
	
	@ManyToOne(fetch = FetchType.LAZY, optional=true)
	@JoinColumn(nullable = true)
	private Picture picture;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = true)
	private User organiser;
	

	@ManyToMany(cascade={CascadeType.PERSIST, CascadeType.DETACH})
	@JoinTable(name="user_event", joinColumns = @JoinColumn(name="event_id"), inverseJoinColumns = @JoinColumn(name="user_id"))
	private Set<User> otherUsers;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(nullable = true)
	private List<Prestation> prestation;
	
	private int nbInscrit;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNbOfParticipant() {
		return nbOfParticipant;
	}

	public void setNbOfParticipant(Integer nbOfParticipant) {
		this.nbOfParticipant = nbOfParticipant;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getAccessibility() {
		return accessibility;
	}

	public void setAccessibility(Boolean accessibility) {
		this.accessibility = accessibility;
	}



	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}
	
	public String getDescriptionEvent() {
		return descriptionEvent;
	}

	public void setDescriptionEvent(String descriptionEvent) {
		this.descriptionEvent = descriptionEvent;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public User getOrganiser() {
		return organiser;
	}

	public void setOrganiser(User organiser) {
		this.organiser = organiser;
	}

	public List<Prestation> getPrestation() {
		return prestation;
	}

	public void setPrestation(List<Prestation> prestation) {
		this.prestation = prestation;
	}

	@Override
	public String toString() {
		return "Event [duration=" + duration + "]";
	}

	public Set<User> getOtherUsers() {
		return otherUsers;
	}

	public void setOtherUsers(Set<User> otherUsers) {
		this.otherUsers = otherUsers;
	}

	public int getNbInscrit() {
		return nbInscrit;
	}

	public void setNbInscrit(int nbInscrit) {
		this.nbInscrit = nbInscrit;
	}
	
	
	
	
}
