package fr.isika.cdi9.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isika.cdi9.model.Duration;
import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.EventType;
import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.model.PrestaType;
import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IDurationRepository;
import fr.isika.cdi9.repository.IEventRepository;
import fr.isika.cdi9.repository.IEventTypeRepository;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.repository.IPrestationRepository;

@Service
public class EventService implements IEventService {
	
	@Autowired
	private IEventRepository repo;
	
	@Autowired
	private IDurationRepository durationRepo;
	
	@Autowired
	private IPrestationRepository prestaRepo;
	
	@Autowired
	private IPlaceRepository placeRepo;
	
	@Autowired
	public IEventTypeRepository eTypeRepo;
		
	@Override
	public void addEvent(Event event) {	
		System.out.println("ok ? : " + event);	
		Duration durationTemp = new Duration();
		durationTemp.setDuration(2f);
		durationTemp.setEndDate(new Date());
		durationTemp.setFormat("jour");
		durationTemp.setStartDate(new Date());
		//Duration durationToSave = durationRepo.save(durationTemp);
		
		Prestation prestationTemp = new Prestation();
		List<Duration> listDuration = new ArrayList<>();
		//listDuration.add(durationToSave);
		prestationTemp.setCapacity(150);
		prestationTemp.setCost(15.0);
		//prestationTemp.setDisponibility(listDuration);
		prestationTemp.setName("traiteur");
		prestationTemp.setPlace(new Place());
	//	prestationTemp.setPrestaType(new PrestaType());
		prestationTemp.setServiceProvider(new User());		
		List<Prestation> listPrestation = new ArrayList<Prestation>();
		List<Prestation> listPrestaTemp = prestaRepo.saveAll(listPrestation);
		
		Place placeTemp = new Place();
		placeTemp.setCity("paris");
		placeTemp.setName("capitale");
		placeTemp.setStreetNbAndName("3 av eglise");
		placeTemp.setZipCode("86");
		Place placeToSave = placeRepo.saveAndFlush(placeTemp);
		
	//	event.setDuration(durationToSave);
		event.setPrestation(listPrestaTemp);
		event.setPlace(placeToSave);
		
		repo.save(event);
	}


	public List<Event> findAllCommingEvent(User user) {	
		
		return repo.findAllCommingEventByUserId(user);
	}
	


	
	
	
}
