package fr.isika.cdi9.services;

import fr.isika.cdi9.model.Event;

public interface IEventService {
	
	void addEvent(Event event);

}
