package fr.isika.cdi9.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.isika.cdi9.CustomUserDetails;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IUserRepository;

public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private IUserRepository repo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = repo.findByEmail(email);
		if (user == null ) {
			throw new UsernameNotFoundException("Utilisateur non trouvé");
		}
		return new CustomUserDetails(user);
	}

}
