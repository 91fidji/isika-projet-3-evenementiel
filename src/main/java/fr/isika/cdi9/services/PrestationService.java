package fr.isika.cdi9.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.repository.IEventTypeRepository;
import fr.isika.cdi9.repository.IPrestationRepository;

@Service
public class PrestationService {

	@Autowired
	public IPrestationRepository prestaRepo;
	
		
	public List<Prestation> findAllCommingPrestation(){		
		return prestaRepo.findAll();	
	}
	
	
	
	
	
	
}
