package fr.isika.cdi9.services;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IRoleRepository;
import fr.isika.cdi9.repository.IUserRepository;

@Service("userService")
public class UserService {
	
	private IUserRepository userRepo;
	
	private IRoleRepository roleRepo;
	
	private BCryptPasswordEncoder bCryptPwdEncoder;
	
	@Autowired
	public UserService(IUserRepository userRepo, 
			IRoleRepository roleRepo, 
			BCryptPasswordEncoder bCryptPwdEncoder) {	
		this.userRepo = userRepo;
		this.roleRepo = roleRepo;
		this.bCryptPwdEncoder = bCryptPwdEncoder;
	}
	
	public User findUserByEmail(String email) {
		return userRepo.findByEmail(email);
	}
	
	public void saveUser(User user) {
		user.setPassword(bCryptPwdEncoder.encode(user.getPassword()));
		System.out.println("account1 " +user);
		
		if ("Organisateur".equalsIgnoreCase(user.getAccountType())) {
			Role userRole = roleRepo.findByRole("Organisateur");
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		}
		
		if ("Admin".equalsIgnoreCase(user.getAccountType())) {
			Role userRole = roleRepo.findByRole("Admin");
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		}
		
		if ("Participant".equalsIgnoreCase(user.getAccountType())) {
			Role userRole = roleRepo.findByRole("Participant");
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		}
		
		if ("Prestataire".equalsIgnoreCase(user.getAccountType())) {
			Role userRole = roleRepo.findByRole("Prestataire");
			user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

		}
		
		user.setActive(1);

		userRepo.save(user);		
	}
	
	public User getById(User user) {
		return userRepo.getById(user.getId());
	}
	
	public void removeUserByUser(User user) {
		userRepo.delete(user);
	}


}
