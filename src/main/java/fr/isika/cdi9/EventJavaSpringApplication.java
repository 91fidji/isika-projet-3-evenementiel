package fr.isika.cdi9;


import java.io.File;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import fr.isika.cdi9.controller.PrestataireController;

@SpringBootApplication
public class EventJavaSpringApplication{

	@Autowired
	private JavaMailSender javaMailSender;
	
	public static void main(String[] args) {

		new File(PrestataireController.uploadDirectory).mkdir(); 
		SpringApplication.run(EventJavaSpringApplication.class, args);
	
	}
	
	void sendEmail() {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo("yelmejjad@gmail.com");
		
		msg.setSubject("Test mail projet 3");
		
		msg.setText("Test reussi !");
		
		javaMailSender.send(msg);
	}
	
	public void run(String... args) throws MessagingException, IOException {
		System.out.println("Sending Email...");
		sendEmail();
		System.out.println("Done");
		
	}

}
