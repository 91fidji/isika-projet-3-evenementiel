package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isika.cdi9.model.Picture;

public interface IPictureRepository extends JpaRepository<Picture, Long> {

}
