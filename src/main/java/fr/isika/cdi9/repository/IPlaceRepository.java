package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isika.cdi9.model.Place;

public interface IPlaceRepository extends JpaRepository<Place, Long> {

}
