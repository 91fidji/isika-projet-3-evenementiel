package fr.isika.cdi9.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.cdi9.model.Prestation;
import fr.isika.cdi9.model.User;

public interface IPrestationRepository extends JpaRepository<Prestation, Long>{

	@Query("SELECT p FROM Prestation p WHERE p.serviceProvider = :user")
	List<Prestation> findPrestaByServiceProviderById(User user);

	@Transactional
	List<Prestation> removeById(Long id);


}

	