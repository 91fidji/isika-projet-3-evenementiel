package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.isika.cdi9.model.EventType;
import fr.isika.cdi9.model.PrestaType;

public interface IEventTypeRepository extends JpaRepository<EventType, Long> {

	@Query("SELECT e FROM EventType e WHERE e.eventType = :eventType")
	public EventType findByEventType(String eventType);
	
}
