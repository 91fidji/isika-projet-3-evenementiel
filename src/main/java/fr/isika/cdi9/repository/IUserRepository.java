package fr.isika.cdi9.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.User;

public interface IUserRepository extends JpaRepository<User, Long> {
	@Query("SELECT u FROM User u WHERE u.email = :email")
	User findByEmail(String email);
	
	@Query("SELECT u from User u inner join Event e WHERE e.otherUsers = :event")
	public List<User> findAllParticipantByEvent(Event event);

}
