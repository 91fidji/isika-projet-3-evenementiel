package fr.isika.cdi9.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.isika.cdi9.model.Event;
import fr.isika.cdi9.model.User;

public interface IEventRepository extends JpaRepository<Event, Long> {

	
	@Query("select e from Event e where e.organiser= :user")
	public List<Event> findAllEventByUserId(User user);
	
	
	@Query("select e from Event e where e.organiser= :user")
	public List<Event> findAllCommingEventByUserId(User user);
	
	 //where e.startingDate > :from and
	@Query("select e from Event e inner join e.otherUsers u where u =:participant")
	public List<Event> findAllEventByParticipantId(User participant);
	


}
