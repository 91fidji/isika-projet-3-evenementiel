package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isika.cdi9.model.Role;

public interface IRoleRepository extends JpaRepository<Role, Long> {
	
	public Role findByRole(String role);

}
