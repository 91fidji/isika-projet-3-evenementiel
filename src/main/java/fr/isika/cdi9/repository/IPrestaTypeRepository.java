package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isika.cdi9.model.PrestaType;



public interface IPrestaTypeRepository extends JpaRepository<PrestaType, Long> {
	
	public PrestaType findByPrestaType(String prestaType);

}
