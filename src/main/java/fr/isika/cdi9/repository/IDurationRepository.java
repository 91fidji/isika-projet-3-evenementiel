package fr.isika.cdi9.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.isika.cdi9.model.Duration;

public interface IDurationRepository extends JpaRepository<Duration, Long> {

}
