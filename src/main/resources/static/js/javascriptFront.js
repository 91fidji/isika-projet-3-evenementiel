const imgSlider = document.querySelector("#slider img");
const leftArrow = document.querySelector(".fa-arrow-left");
const rigtArrow = document.querySelector(".fa-arrow-right");
let index = 0;



//cration d'un tableau d'objets contenant des images : 
const imagesTable = [
    {src:"carousel1.jpg", alt:"Street Art"},
    {src:"carousel2.jpg", alt:"Fast Lane"}, 
    {src:"carousel3.jpg", alt:"Street Art"},
    {src:"carousel4.jpg", alt:"Fast Lane"}, 
    {src:"carousel5.jpg", alt:"Street Art"},
    {src:"carousel6.jpg", alt:"Fast Lane"}, 

];

function changeImage()  {
    
    window.setInterval(() =>{
        imgSlider.src= "img/"+ imagesTable[index].src;
        index++;
        if (index === imagesTable.length) {
            index = 0;
        }
    }, 3000);
}

function next(){
    if (index === 0) {
        index = imagesTable.length;
    }
    else{
        index--;
    }
    imgSlider.src="img/"+imagesTable[index].src;
}

//execution
changeImage();
leftArrow.addEventListener("click",next)


