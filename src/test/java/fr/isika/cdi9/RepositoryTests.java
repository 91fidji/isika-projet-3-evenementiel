package fr.isika.cdi9;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import fr.isika.cdi9.controller.PlaceController;
import fr.isika.cdi9.model.Place;
import fr.isika.cdi9.model.Role;
import fr.isika.cdi9.model.User;
import fr.isika.cdi9.repository.IPlaceRepository;
import fr.isika.cdi9.services.UserService;

@SpringBootTest
class RepositoryTests {
	@Autowired
	PlaceController placeController;
	
	@Autowired
	IPlaceRepository placeRepo;
	
	@Autowired
	UserService userService;
	
	@Test
	void contextLoads() throws Exception {
		assertNotNull(placeController, "controller not null");
	}
	
	@Test
	@DisplayName("SavePlace")

	void shouldSavePlace() {
		Place place = new Place();
		place.setCity("Paris");
		place.setId(1l);
		place.setName("main");
		place.setStreetNbAndName("12");
		place.setZipCode("75");
		
		placeRepo.save(place);
		Place placeSaved = placeRepo.getById(place.getId());
		/*
		 * assertEquals(placeSaved.getCity(),place.getCity());
		 */		
		assertNotNull(placeSaved, "place not null");

	}
	
	@Test
	@Transactional
	@DisplayName("UserService")
	void shouldVerifyUserCreation() {
		Role roleParticipant = new Role();
		HashSet<Role> roles = new HashSet<Role>();
		roles.add(roleParticipant);
		User participant = new User();
		participant.setId(1l);
		participant.setActive(1);
		participant.setEmail("participant1@okheey.fr");
		participant.setPassword("participant");
		participant.setRoles(roles);
		participant.setLastname("p");
		participant.setFirstname("participant");
		participant.setPhone("0141011312");
		Place placeParticipant = new Place();
		placeParticipant.setCity("Lyon");
		placeParticipant.setName("Main");
		placeParticipant.setStreetNbAndName("13");
		placeParticipant.setZipCode("69");
		participant.setPlace(placeRepo.save(placeParticipant));
		userService.saveUser(participant);
		
		User userToTestById = userService.getById(participant);

		User userToTest1ByEmail = userService.findUserByEmail("participant1@okheey.fr");
		assertEquals(participant, userToTest1ByEmail);
		assertEquals(participant, userToTestById);

		userService.removeUserByUser(participant);
	
	}
	
	@Test
	@Transactional
	@DisplayName("User Service Not Null")
	void shouldVerifyUserCreationIsNotNull() {
		Role roleParticipant = new Role();
		HashSet<Role> roles = new HashSet<Role>();
		roles.add(roleParticipant);
		User admin = new User();
		admin.setId(1l);
		admin.setActive(1);
		admin.setEmail("test@okheey.fr");
		admin.setPassword("participant");
		admin.setRoles(roles);
		admin.setLastname("p");
		admin.setFirstname("participant");
		admin.setPhone("0141019312");
		Place placeParticipant = new Place();
		placeParticipant.setCity("Marse1lle");
		placeParticipant.setName("Main");
		placeParticipant.setStreetNbAndName("13");
		placeParticipant.setZipCode("69");
		admin.setPlace(placeRepo.save(placeParticipant));
		userService.saveUser(admin);

		User userToTest1ByEmail = userService.findUserByEmail("test@okheey.fr");
		assertNotNull(userToTest1ByEmail);
		userService.removeUserByUser(admin);
	
	}
}
	
	

